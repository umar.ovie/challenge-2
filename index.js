const { rawListeners } = require("process");
const readline = require("readline");
const interface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function input(question) {
  return new Promise((resolve) => {
    interface.question(question, (data) => {
      resolve(data);
    });
  });
}
function sorting(data) {
  for (let a = 0; a < data.length - 1; a++) {
    for (let b = a + 1; b < data.length; b++) {
      if (data[a] > data[b]) {
        let temp = data[a];
        data[a] = data[b];
        data[b] = temp;
      }
    }
  }
  return data;
}
function cekLulus(data) {
  const value = [];
  let a = 0;
  let b = 0;
  data.filter((element) => {
    if (element >= 60) {
      a++;
    } else {
      b++;
    }
  });
  value.push(a);
  value.push(b);
  return value;
}

function display(data, param) {
  let a = 0;
  data.filter((element) => {
    if (element == param) {
      a = param;
    }
  });
  if (a != param) {
    return "Nilai tidak ditemukan";
  }
  return a;
}
function avg(data) {
  const result = data.reduce((a,b)=> a+b,0);
  return result/data.length;
}
async function main() {
  const datanya = [];
  let run = true;
  while (run) {
    const data = await input("Masukkan angka: ");
    if (data == "q") {
      interface.close();
      run = false;
    } else {
      datanya.push(+data);
    }
  }
  const result = sorting(datanya);
  const cek = cekLulus(result);
  console.clear();
  console.log(`
Nilai tertinggi : ${result[result.length - 1]}
Nilai terendah : ${result[0]}
Rata-Rata : ${avg(result)}
Siswa lulus : ${cek[0]}, Siswa tidak lulus : ${cek[1]}
Urutan nilai dari terendah ke tertinggi : ${result.join()}
Siswa nilai 90 dan nilai 100 : ${display(result, 90)}, ${display(result, 100)} 
    `);
}

main();